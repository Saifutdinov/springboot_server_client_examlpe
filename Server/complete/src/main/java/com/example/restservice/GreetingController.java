package com.example.restservice;
import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {
	private ListCars cars = new ListCars();

	@GetMapping("/car/{id}")
	public Car get(@PathVariable long id) {
		return cars.getCarById(id);
	}

	@GetMapping("/cars")
	public ListCars getCars() {
		return cars;
	}

	@PostMapping("/car")
	public void add(@RequestBody Car car) {
		cars.add(car);
	}

	@PutMapping("/car")
	public void update(@RequestBody Car car) {
		Car currentCar = cars.getCarById(car.getId());
		if(currentCar != null)
			currentCar.update(car);
	}
}
