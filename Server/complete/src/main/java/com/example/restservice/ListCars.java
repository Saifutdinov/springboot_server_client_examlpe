package com.example.restservice;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ListCars {
    private final AtomicLong counter = new AtomicLong();

    public List<Car> cars;

    public ListCars() {
        this.cars = new LinkedList();
    }

    public ListCars(List<Car> cars) {
        this.cars = cars;
    }

    public void add(Car car){
        cars.add(new Car(counter.incrementAndGet(), car.getKolesa(), car.getProswet()));
    }

    public Car getCarById(long id){
        for (Car car:cars) {
            if(car.getId() == id)
                return car;
        }
        return null;
    }
}
