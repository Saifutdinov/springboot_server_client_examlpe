package com.example.consumingrest;

import java.util.HashMap;
import java.util.List;

public class ListCars {
    public HashMap<Integer,Car> cars;

    public ListCars(HashMap<Integer,Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "ListCars{" +
                "cars=" + cars +
                '}';
    }

    public ListCars() {
    }
}
