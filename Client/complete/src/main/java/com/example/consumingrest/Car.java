package com.example.consumingrest;

public class Car {
    private long id;
    private int kolesa;
    private int proswet;

    public Car(long id, int kolesa, int proswet) {
        this.id = id;
        this.kolesa = kolesa;
        this.proswet = proswet;
    }

    public Car(int kolesa, int proswet) {
        this.kolesa = kolesa;
        this.proswet = proswet;
    }

    public Car() {
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", kolesa=" + kolesa +
                ", proswet=" + proswet +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getKolesa() {
        return kolesa;
    }

    public void setKolesa(int kolesa) {
        this.kolesa = kolesa;
    }

    public int getProswet() {
        return proswet;
    }

    public void setProswet(int proswet) {
        this.proswet = proswet;
    }
}