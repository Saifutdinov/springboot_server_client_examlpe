package com.example.consumingrest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;

@SpringBootApplication
public class ConsumingRestApplication {

	private static final Logger log = LoggerFactory.getLogger(ConsumingRestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ConsumingRestApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {

		return args -> {
			ReadAndLogCars(restTemplate);
			AddCar(restTemplate, 5,2);
			ReadAndLogCars(restTemplate);
			AddCar(restTemplate, 3,2);
			ReadAndLogCars(restTemplate);
			ReadAndLogCarById(restTemplate,1);
			UpdateCar(restTemplate, 2, 10,10);
			ReadAndLogCarById(restTemplate,2);
		};
	}
	void ReadAndLogCars(RestTemplate restTemplate){
		ListCars cars = restTemplate.getForObject(
				"http://localhost:9090/cars", ListCars.class);
		log.info(cars.toString());
	}

	void ReadAndLogCarById(RestTemplate restTemplate, long id){
		Car car = restTemplate.getForObject(
				"http://localhost:9090/car/"+id, Car.class);
		log.info(car.toString());
	}

	void AddCar(RestTemplate restTemplate, int k, int p){
		restTemplate.postForLocation(
				"http://localhost:9090/car", new Car(k, p));
	}

	void UpdateCar(RestTemplate restTemplate, long id, int k, int p){
		restTemplate.put(
				"http://localhost:9090/car", new Car(id, k, p));
	}
}
